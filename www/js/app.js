// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform,$rootScope,$location,$ionicPopup) {
 $rootScope.showheader=true
/*************** forget password ****************/
     $rootScope.forget_password=function (){
        $ionicPopup.show({
        template: 'Enter your email address below.<label class="item item-input" style="  height: 34px; margin-top: 10px;"><input  type="email"  /></label>',
        title: 'Forget Password',
        subTitle: ' ',
        scope: $rootScope,
        buttons: [
        {text: 'Send',
        type: 'button-block main-btn first-btn main-bg-color'},
        { text: 'Cancel' ,
        type: 'button-block main-btn second-btn sec-main-bg-color'},]
        });
    };

/**************** location function ****************/
   $rootScope.goto=function(url){
        $location.path(url)
    }
/************** favorite function *************/
   $rootScope.color_favorite=function(){
   $rootScope.favorite=!$rootScope.favorite;
   }
   $rootScope.favorite=false;
/************** active function *************/
   $rootScope.activeiItem=function(index){
   $rootScope.active=index;
   }
/************** social media function *************/
   $rootScope.showSocial=function(){
   $rootScope.social=!$rootScope.social;
   }
   $rootScope.social=false;
/**************** array list ****************/
    $rootScope.cat=
      [
        // {id:"1",img:"img/01.png",bg:"img/bg1.png", icon:'globe-americas', title:"IDIOMA", href: '#/app/list'},
      // {id:"3",img:"img/03.png",bg:"img/bg3.png", icon:'volume-up' ,title:"AUDIOS", href: '#/app/list'},
      // {id:"2",img:"img/02.png",bg:"img/bg2.png", icon: 'film', title:"VIDEOS", href: '#/app/list'},
      {id:"4",img:"img/04.png",bg:"img/bg4.png", icon:'book', title:"CONTENIDO", href: '#/app/unidades'},
      {id:"5",img:"img/05.png",bg:"img/bg1.png", icon:'keyboard', title:"TEST", href: '#/app/test'},
      {id:"6",img:"img/06.png",bg:"img/bg2.png", icon:'info', title:"ACERCA DE", href: '#/app/about'}
      ];

    $rootScope.list=[
      {id:"1",img:"https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Palenque_glyphs-edit1.jpg/300px-Palenque_glyphs-edit1.jpg",price:"5",title:"Numero de 1 a 100"},
      {id:"2",img:"https://i.ytimg.com/vi/J6WZ37qVqNg/maxresdefault.jpg",price:"15",title:"Palabras comunes"},
    {id:"3" ,img:"https://aprende.guatemala.com/wp-content/uploads/2018/01/Origen-de-la-palabra-Maya-en-Guatemala.jpg",price:"25",title:"Historia ..."},
      {id:"4",img:"img/001.png",price:"265",title:"Bootstrap Layouts -  responsive single Page Design"},{id:"5",img:"img/002.png",price:"265",title:"Bootstrap Layouts -  responsive single Page Design"},
    {id:"6" ,img:"img/003.png",price:"315",title:"jQuery for Web Designers"}]

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {

  $ionicConfigProvider.backButton.text('').previousTitleText('');
  $ionicConfigProvider.navBar.alignTitle('center');

  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })
  .state('login', {
    url: "/login",
        templateUrl: "templates/login.html"
  })

  .state('register', {
    url: "/register",
        templateUrl: "templates/register.html"
  })

  .state('app.home', {
    url: "/home",
    views: {
      'menuContent': {
        templateUrl: "templates/home.html"
      }
    }
  })
    .state('app.list', {
      url: "/list",
      views: {
        'menuContent': {
          templateUrl: "templates/list.html"
        }
      }
    })
    .state('app.list_unidades', {
      url: "/unidades",
      views: {
        'menuContent': {
          templateUrl: "templates/contenido/list_unidades.html",
          controller: 'UnidadesCtrl'
        }
      }
    })
    .state('app.detalle_unidad', {
      url: "/unidad",
      views: {
        'menuContent': {
          templateUrl: "templates/contenido/detalle_unidad.html",
          controller: 'DetalleUnidadCtrl'
        }
      },
      params:{
        unidad:"1"
      }
    })
  .state('app.details', {
    url: "/details",
    views: {
      'menuContent': {
        templateUrl: "templates/details.html"
      }
    }
  })
  .state('app.contact', {
    url: "/contact",
    views: {
      'menuContent': {
        templateUrl: "templates/contact.html"
      }
    }
  })
    .state('app.about', {
      url: "/about",
      views: {
        'menuContent': {
          templateUrl: "templates/about.html"
        }
      }
    })

    .state('app.test', {
      url: "/test",
      views: {
        'menuContent': {
          templateUrl: "templates/test/unidades.html",
          controller: 'UnidadesTestCtrl'
        }
      }
    })
    .state('app.test_unidad', {
      url: "/test_unidad",
      views: {
        'menuContent': {
          templateUrl: "templates/test/detalle.html",
          controller: 'DeltalleTestCtrl'
        }
      },
      params:{
        unidad:"1"
      }
    })
  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "templates/profile.html",
          controller:"profileCtrl"
      }
    }
  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
})

.controller('profileCtrl', function($scope, $rootScope) {
    $scope.$on('$ionicView.beforeEnter', function() {
       $rootScope.showheader=false
   })
$scope.$on('$ionicView.beforeLeave', function() {
       $rootScope.showheader=true
   })

})
