app.controller('ContenidoCtrl', function($scope, $stateParams) {
});

app.controller('UnidadesTestCtrl', function($scope,$state,  $stateParams) {
  $scope.listUnidades = {
    "1": "Unidad 1 - La escritura del idioma Mam",
  };
  $scope.irUnidad = (indice)=>{
    $state.go('app.test_unidad', {
      unidad: indice
    });
  }
});

app.controller('UnidadesCtrl', function($scope,$state,  $stateParams) {
  $scope.listUnidades = {
    "1": "Unidad 1 - La escritura del idioma Mam",
    "2": "Unidad 2 - Tipos de palabras",
    "3": "Unidad 3 - La oración",
  };
  $scope.irUnidad = (indice)=>{
    $state.go('app.detalle_unidad', {
      unidad: indice
    });
  }
});

app.controller('DetalleUnidadCtrl', function($scope, $state, $stateParams,$ionicModal) {
  $scope.title= "Detalle unidades"
  console.log("State: ",$stateParams)
  $scope.unidad_index = $stateParams.unidad
  $scope.contenidoModal = {}
  $scope.listTemas = window.contenidoJson
  $scope.indiceTarjeta = 0;
  $scope.tarjeta = {}

  $scope.verContenido = (contenido)=>{
    console.log(contenido)
    $scope.contenidoModal = contenido
    if($scope.contenidoModal.tarjetas && $scope.contenidoModal.tarjetas.length>0){
      $scope.tarjeta = $scope.contenidoModal.tarjetas[$scope.indiceTarjeta]
    }
    $scope.modal.show();
  }
  $scope.onSwipeRight = ()=>{
    console.log("deslizar a la derecha")

    if($scope.indiceTarjeta>0){
      $scope.indiceTarjeta --;
      $scope.tarjeta = $scope.contenidoModal.tarjetas[$scope.indiceTarjeta]
    }
  }

  $scope.onSwipeLeft = ()=>{
    console.log("deslizar a la izquierd")
    if($scope.indiceTarjeta<$scope.contenidoModal.tarjetas.length-1){
      $scope.indiceTarjeta ++;
      $scope.tarjeta = $scope.contenidoModal.tarjetas[$scope.indiceTarjeta]
    }
  }

  $ionicModal.fromTemplateUrl('contenido-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
}
);



app.controller('DeltalleTestCtrl', function($scope, $state, $stateParams,$ionicModal) {
  $scope.title= "Detalle unidades"
  console.log("State: ",$stateParams)
  $scope.unidad_index = $stateParams.unidad
  $scope.contenidoModal = {}
  $scope.listadoTest = window.contenidoTestJson
  $scope.indiceTarjeta = 0;
  $scope.tarjeta_multiple = {}
  $scope.tarjeta_traducir = {}
  $scope.test_finalizado = false;

  window.$scope = $scope
  $scope.verContenido = (contenido)=>{
    console.log(contenido)
    $scope.contenidoModal = contenido
    $scope.modal.show();
  }
  $scope.onSwipeRight = ()=>{
    console.log("deslizar a la derecha")

    if($scope.indiceTarjeta>0){
      $scope.indiceTarjeta --;
    }
  }

  $scope.onSwipeLeft = ()=>{
    console.log("deslizar a la izquierd")
    let max_item = 0;
    if($scope.contenidoModal.multiple && $scope.contenidoModal.multiple.length){
      max_item = $scope.contenidoModal.multiple.length
    }

    if($scope.contenidoModal.traducir && $scope.contenidoModal.traducir.length){
      max_item = $scope.contenidoModal.traducir.length
    }
    if($scope.indiceTarjeta<= max_item){
      $scope.indiceTarjeta ++;
      if($scope.indiceTarjeta== max_item){
        $scope.test_finalizado = true;
      }else{
        $scope.test_finalizado = false;
      }
      }
  }

  $ionicModal.fromTemplateUrl('contenido-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $scope.calcularBuenas = function(test){
    console.log(test)
    $scope.punteo_final = 0;
    for(let item of test){
      if(item.respuesta == item.respuesta_usuario){
        $scope.punteo_final +=1
      }
    }
    return $scope.punteo_final;
  }

  $scope.volverTest = function(){
    $scope.indiceTarjeta = 0;
    $scope.test_finalizado = false;
  }
}
);
