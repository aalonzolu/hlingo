window.contenidoJson = {
  "2": {
    titulo: "Tipos de palabras",
    descripcion: "En esta unidad se trabaja el nivel morfológico, es decir las reglas para la formación de palabras y la clasificación de estas a efecto de iniciar la expresión y la comunicación a través del lenguaje articulado. Acá, se abordan las palabras y los diversos tipos de afijos (prefijos y sufijos)",
    articulos: [
      {
        titulo: "Sustantivos",
        contenido: `<p>Lea la siguiente Historia</p><p><strong>B&rsquo;ib&rsquo;aj</strong></p><p><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574056304394-1574056304394.png" style="width: 3em;" class="fr-fic fr-dib"></p><p><br></p><p>En la historia anterior encontramos resaltadas las palabras <em>txub&rsquo;aj, q&rsquo;a, xul, chib&rsquo;j, q&rsquo;ij, pintze, chi&rsquo;l, ja, pich&rsquo;, ku&rsquo;k, b&rsquo;ech, tze&nbsp;</em>que indican personas, animales, lugares, etc. A estas palabras se les llama <strong>sustantivos.</strong></p><p>A continuaci&oacute;n otros ejemplos de sustantivos:</p><p><br></p><dl><dl><dl><dl><dd><table cellpadding="7" cellspacing="0" width="260"><tbody><tr><td height="12" width="28.110599078341014%"><p>ch&rsquo;ech&rsquo;</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>mecate</p></td></tr><tr><td height="21" width="28.110599078341014%"><p>tz&rsquo;utz&rsquo;</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>azad&oacute;n</p></td></tr><tr><td height="21" width="28.110599078341014%"><p>witz</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>cerro</p></td></tr><tr><td height="12" width="28.110599078341014%"><p>ka&rsquo;</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>piedra de moler</p></td></tr></tbody></table></dd></dl></dl></dl></dl><p><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574056470873-1574056470873.png" style="width: 60%;" class="fr-fic fr-dib"></p><p>A continuaci&oacute;n otros ejemplos de sustantivos:</p><dl><dl><dl><dl><dd><table cellpadding="7" cellspacing="0" width="260"><tbody><tr><td height="12" width="28.110599078341014%"><p>ch&rsquo;ech&rsquo;</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>mecate</p></td></tr><tr><td height="21" width="28.110599078341014%"><p>tz&rsquo;utz&rsquo;</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>azad&oacute;n</p></td></tr><tr><td height="21" width="28.110599078341014%"><p>witz</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>cerro</p></td></tr><tr><td height="12" width="28.110599078341014%"><p>ka&rsquo;</p></td><td width="16.589861751152075%"><p>=</p></td><td width="55.29953917050691%"><p>piedra de moler</p></td></tr></tbody></table></dd></dl></dl></dl></dl><p><br></p>`
      },
      {
        titulo: "Sustantivo Funcionando como núcleo del sujeto",
        contenido: `<p><br></p><p><br></p><p><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574056666208-1574056666208.png" style="width: 300px;" class="fr-fic fr-dib"></p><p><br></p><p><br></p><p><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574056686078-1574056686078.png" style="width: 300px;" class="fr-fic fr-dib"></p><p><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574056694509-1574056694509.png" style="width: 300px;" class="fr-fic fr-dib"></p><p><strong>Ejemplo de objeto directo</strong></p><p><strong><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574056716573-1574056716573.png" style="width: 300px;" class="fr-fic fr-dib"></strong></p><p style='margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0in;'><strong>Ejemplo de objeto indirecto</strong></p><p style='margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0in;'><br></p><p style='margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0in;'>Ma tzaj tloq&rsquo;o&rsquo;n txub&rsquo;aj jun klob&rsquo;j <u>te txin</u>.</p><p style='margin-bottom: 0in;line-height: 156%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 2.38in;margin-top: 0.11in;'><strong>Objeto indirecto&nbsp;</strong></p><p style='margin-bottom: 0in;line-height: 156%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 2.38in;margin-top: 0.11in;'>La mam&aacute; le compr&oacute; un g&uuml;ipil a ni&ntilde;a.</p><p style='margin-bottom: 0in;line-height: 156%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 2.38in;margin-top: 0.11in;'><br></p><p style='margin-bottom: 0in;line-height: 156%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 2.38in;margin-top: 0.11in;'><strong>Ejemplo de adjunto</strong></p><p style='margin-bottom: 0.11in;line-height: 0.19in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;'>Nt&rsquo;ikypaj ẍiky <u>toj chq&rsquo;ajlaj.</u></p><p style='margin-bottom: 0.11in;line-height: 0.3in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 0in;margin-top: 0.02in;'><strong>Adjunto de lugar&nbsp;</strong></p><p style='margin-bottom: 0.11in;line-height: 0.3in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 0in;margin-top: 0.02in;'>El conejo salta en la pradera.</p><p><br></p><p><br></p>`
      },
      {
        titulo: "Posesión de Sustantivos",
        contenido: `<p style='margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.04in;'>Lea las siguientes oraciones:</p><p style='margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.04in;'><br></p><ol><li><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;">Ma ch&rsquo;iy<strong>t</strong>kojinLoẍ. La milpa de Alonzo yacreci&oacute;.</p></li></ol><ol start="2"><li><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;">Ma tzaj tloq&rsquo;on nanjun<strong>t</strong>kolob&rsquo;a. Mam&aacute; te compr&oacute; ung&uuml;ipil.</p></li></ol><ol start="3"><li><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;">Nchitzalajk&rsquo;walti&rsquo;j<strong>ky</strong>xajab&rsquo;. Los ni&ntilde;os y las ni&ntilde;as est&aacute;n felices por sus zapatos.</p></li></ol><ol start="4"><li><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;">Matzajo&rsquo;yinjun<strong>n</strong>b&rsquo;eche. Me regalaron una flor</p></li></ol><p style='margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;'><br></p><p style='margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;'><br></p><p style='margin-bottom: 0.11in;line-height: 0.19in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;'>F&iacute;jese bien en las palabras que aparecen en las oraciones anteriores, a continuaci&oacute;n se</p><p style='margin-bottom: 0.11in;line-height: 0.24in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.01in;'>le presentan las mismas en dos formas: una en su forma no pose&iacute;da y la otra en su forma pose&iacute;da.</p><p style='margin-bottom: 0.11in;line-height: 0.24in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.01in;'><br></p><p style='margin-bottom: 0.11in;line-height: 0.24in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.01in;'><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574056897451-1574056897451.png" style="width: 300px;" class="fr-fic fr-dib"></p><p style='margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;font-family: "Arial", serif;font-size:16px;margin-right: 1in;line-height: 130%;'>Las palabras pose&iacute;das inician con una letra, la cual est&aacute; resaltada, cada una de ellas tiene significado como <em>su</em>, (de &eacute;l/ella), <em>su&nbsp;</em>(de ellos/ellas), <em>tu</em>, <em>mi&nbsp;</em>e indican, qui&eacute;n es el due&ntilde;o.</p>`
      },
      {
        titulo: "Pluralización de Sustantivos",
        contenido: `<p><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574057080138-1574057080138.png" style="width: 300px;" class="fr-fic fr-dib"></p><p><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574057085543-1574057085543.png" style="width: 300px;" class="fr-fic fr-dib"></p><p style='margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;font-family: "Arial", serif;font-size:16px;margin-right: 1in;line-height: 130%;'>F&iacute;jese en los sujetos de las dos oraciones. En la primera es un solo ni&ntilde;o el que estudia, por lo tanto <strong>el</strong><strong>&nbsp;</strong><strong>sujeto</strong><strong>&nbsp;</strong><strong>es</strong><strong>&nbsp;</strong><strong>singular</strong>. En la segunda oraci&oacute;n hay varios ni&ntilde;os que estudian, entonces <strong>el sujeto es plural</strong>, pero en los dos casos la palabra <em>&ldquo;k&rsquo;wal&rdquo;&nbsp;</em>(ni&ntilde;o) no est&aacute; pluralizado y lo que hace la diferencia es el marcador de persona <em><strong>chi&nbsp;</strong></em>que en algunos casos puede ser solo <em><strong>i</strong></em>. En las otras dos figuras, en una encontramos un solo perro y en la otra varios perros, eso significa que uno es singular y el otro es plural, en &eacute;ste caso la diferencia se hace a trav&eacute;s de la part&iacute;cula <em><strong>qe&rsquo;</strong></em>.</p><p style='margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;font-family: "Arial", serif;font-size:16px;margin-right: 1in;line-height: 130%;'><br></p><p style='margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;font-family: "Arial", serif;font-size:16px;margin-right: 1in;line-height: 130%;'><img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1574057105468-1574057105468.png" style="width: 300px;" class="fr-fic fr-dib"></p>`
      },
      {
        titulo: "Clasificación de sustantivos",
        contenido: `<p style='margin-bottom: 0in;line-height: 130%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 0.94in;margin-top: 0.13in;'>De acuerdo con los cambios que presentan al ser pose&iacute;dos, como tambi&eacute;n en su composici&oacute;n, los sustantivos se clasifican en:</p><p style='margin-bottom: 0in;line-height: 130%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 0.94in;margin-top: 0.06in;'><br></p><ol type="a"><li><p style='margin-bottom: 0in;line-height: 130%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 0.94in;margin-top: 0.06in;'>Sustantivos seg&uacute;n posesi&oacute;n</p></li></ol><p style='margin-bottom: 0in;line-height: 130%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 0.94in;margin-top: 0.06in;'>Estos tienen que ver con los cambios que sufren cuando son pose&iacute;dos, de &eacute;stos, los subgrupos que resultan son:</p><p style='margin-bottom: 0.11in;line-height: 0.19in;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;'><strong>Sustantivos invariables:&nbsp;</strong>estos no cambian su forma al ser pose&iacute;dos.</p><p style='margin-bottom: 0.11in;line-height: 108%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.13in;'>Ejemplos:</p><table cellpadding="7" cellspacing="0" width="546"><tbody><tr><td height="25" style="border: none;padding: 0in;" width="18.73727087576375%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;">Xk&rsquo;utz&rsquo;ib&rsquo;</p></td><td style="border: none;padding: 0in;" width="29.124236252545824%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.41in;">computadora</p></td><td style="border: none;padding: 0in;" width="24.43991853360489%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.25in;">nxk&rsquo;utz&rsquo;ib&rsquo;a</p></td><td style="border: none;padding: 0in;" width="27.69857433808554%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.33in;">mi computadora</p></td></tr><tr><td height="47" style="border: none;padding: 0in;" width="18.73727087576375%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;margin-top: 0in;">Tz&rsquo;isb&rsquo;il</p></td><td style="border: none;padding: 0in;" width="29.124236252545824%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.41in;margin-top: 0in;">escoba</p></td><td style="border: none;padding: 0in;" width="24.43991853360489%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.25in;margin-top: 0in;">ntz&rsquo;isb&rsquo;ila</p></td><td style="border: none;padding: 0in;" width="27.69857433808554%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.33in;margin-top: 0in;">mi escoba</p></td></tr><tr><td height="25" style="border: none;padding: 0in;" width="18.73727087576375%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;">Tz&rsquo;ib&rsquo;b&rsquo;il</p></td><td style="border: none;padding: 0in;" width="29.124236252545824%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.41in;">lapicero</p></td><td style="border: none;padding: 0in;" width="24.43991853360489%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.25in;">ntz&rsquo;ib&rsquo;b&rsquo;ila</p></td><td style="border: none;padding: 0in;" width="27.69857433808554%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-top: 0in;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.33in;">mi lapicero</p></td></tr></tbody></table><p style='margin-bottom: 0.11in;line-height: 108%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.13in;'><br><br></p><ul><li><p style='margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;font-family: "Arial", serif;font-size:16px;line-height: 0.26in;'><strong>Inalienables:</strong>estossustantivosterminanconelsufijo<strong>&ndash;b&rsquo;aj</strong>o&ndash;<strong>aj</strong>cuandonoest&aacute;n pose&iacute;dos<strong>.&nbsp;</strong>Una vez que se poseen pierden el sufijo en menci&oacute;n. Una buena parte de estos sustantivos son partes del cuerpo humano o animal, otros son vestimenta, alimentos y algunos designan familiares.</p></li></ul><p style='margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;font-family: "Arial", serif;font-size:16px;margin-left: 0.25in;line-height: 0.26in;'><br></p><table cellpadding="7" cellspacing="0" width="372"><tbody><tr><td height="22" style="border: none;padding: 0in;" width="27.61627906976744%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;">Ejemplos:</p></td><td colspan="3" style="border: none;padding: 0in;" width="72.38372093023256%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;"><br></p></td></tr><tr><td height="28" style="border: none;padding: 0in;" width="29.968454258675077%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;">txa&rsquo;nb&rsquo;aj</p></td><td style="border: none;padding: 0in;" width="27.444794952681388%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.37in;">nariz</p></td><td style="border: none;padding: 0in;" width="20.50473186119874%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.09in;">ntxa&rsquo;ne</p></td><td style="border: none;padding: 0in;" width="22.082018927444796%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;"><br></p><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.09in;">mi nariz</p></td></tr><tr><td height="17" style="border: none;padding: 0in;" width="29.968454258675077%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;margin-top: 0.06in;">ch&rsquo;ekyb&rsquo;aj</p></td><td style="border: none;padding: 0in;" width="27.444794952681388%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.37in;margin-top: 0.06in;">rodilla</p></td><td style="border: none;padding: 0in;" width="20.50473186119874%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.09in;margin-top: 0.06in;">nch&rsquo;ekye</p></td><td style="border: none;padding: 0in;" width="22.082018927444796%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.07in;margin-top: 0.06in;">mi rodilla</p></td></tr><tr><td height="16" style="border: none;padding: 0in;" width="29.968454258675077%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;margin-top: 0.06in;">muxb&rsquo;aj</p></td><td style="border: none;padding: 0in;" width="27.444794952681388%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.37in;margin-top: 0.06in;">ombligo</p></td><td style="border: none;padding: 0in;" width="20.50473186119874%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.14in;margin-top: 0.06in;">tmuxa</p></td><td style="border: none;padding: 0in;" width="22.082018927444796%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.07in;margin-top: 0.06in;">tu ombligo</p></td></tr><tr><td height="16" style="border: none;padding: 0in;" width="29.968454258675077%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;margin-top: 0.06in;">ẍkyinb&rsquo;aj</p></td><td style="border: none;padding: 0in;" width="27.444794952681388%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.37in;margin-top: 0.06in;">oreja</p></td><td style="border: none;padding: 0in;" width="20.50473186119874%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.14in;margin-top: 0.06in;">tẍkyin</p></td><td style="border: none;padding: 0in;" width="22.082018927444796%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.07in;margin-top: 0.06in;">su oreja</p></td></tr><tr><td height="10" style="border: none;padding: 0in;" width="29.968454258675077%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.03in;margin-top: 0.06in;">amaj</p></td><td style="border: none;padding: 0in;" width="27.444794952681388%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.37in;margin-top: 0.06in;">corte</p></td><td style="border: none;padding: 0in;" width="20.50473186119874%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.14in;margin-top: 0.06in;">wame</p></td><td style="border: none;padding: 0in;" width="22.082018927444796%"><p style="margin-bottom: 0in;line-height: 100%;text-align: left;background: transparent;margin-left: 0.07in;margin-top: 0.06in;">mi corte</p></td></tr></tbody></table><p style='margin-bottom: 0.11in;line-height: 108%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.13in;'><br><br></p><ul><li><p style="margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;margin-right: 1in;line-height: 128%;"><strong>Siempre pose&iacute;dos</strong>: son sustantivos que para tener significado deben estar pose&iacute;dos;esdecir,quenofuncionancomopalabrassueltas.Generalmenteest&aacute;n pose&iacute;dos en tercerapersona.</p></li></ul><p style='margin-bottom: 0in;text-align: justify;background: transparent none repeat scroll 0% 0%;font-family: "Arial", serif;font-size:16px;margin-right: 1in;line-height: 128%;'><br></p><p style='margin-bottom: 0in;line-height: 128%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-right: 1in;'><br></p><p style='margin-bottom: 0.11in;line-height: 108%;text-align: left;background: transparent;font-family: "Arial", serif;font-size:16px;margin-top: 0.13in;'><br></p>`
      }
    ]
  },
  "3": {
    titulo: "La oración",
    descripcion: "Contenido en proceso",
    articulos: [
      
    ]
  },
  "4": {
    titulo: "Lexicología",
    descripcion: "Contenido en proceso",
    articulos: [
      
    ]
  },
  "5": {
    titulo: "Pragmática",
    descripcion: "Contenido en proceso",
    articulos: [
      
    ]
  },
  "1": {
    titulo: 'La escritura del idioma Mam',
    descripcion: 'Esta unidad aborda los principales elementos importantes para la iniciación de la escritura del idioma Mam, tales como el alfabeto, acá se inicia con los fonemas propios del idioma y sus respectivos grafemas, ofreciendo una amplia gama de ejemplos y ejercicios, de tal modo que el lector pueda hacer la relación y un proceso de transferencia entre el alfabeto del idioma español con el que la mayoría de Mam hablantes ha aprendido a leer y a escribir. Seguidamente se encontrarán los signos de puntuación para ser tomados en cuenta en la redacción.',
    articulos: [
      {
        titulo: 'El alfabeto del idioma Mam',
        contenido:'Para un mejor entendimiento, el alfabeto del idioma Mam se dividir en: vocales, consonantes simples, consonantes simples glotalizadas, consonantes compuestas sin glotal y consonantes compuestas glotalizadas.<br>' +
          '¿Por qué el saltillo o cierre glotal se incluye dentro del  alfabeto Mam como una consonante más y no como una consonante más y no como parte de una vocal? No es parte de la vocal porque las vocales no se pronuncian con la glotis cerrada, lo que sí ocurre es que después de una vocal la glotis se cierra. El saltillo representa un momento en el que el aire se detiene, para producir determinado sonido, como el caso de las consonantes mencionadas arriba.',
        imagenes: [
          'img/contenido/consonantes.png',
          'img/contenido/consonantes-simples.png',
          'img/contenido/consonantes-simples-g.png',
          'img/contenido/Picture3.png',
        ]
      },

      {titulo: 'El nombre de las letras ',
        contenido:'El nombre de las vocales es el mismo sonido de cada una.<br>' +
          '<br>' +
          'a=a\te=e\ti=i\to=o\tu=u<br>' +
          '<br>' +
          'El nombre de las consonantes se forma agregando la vocal /e/.',
        imagenes: [
          'img/contenido/nombre-le.png'
        ]
      },
      {titulo:
          'Las Vocales',
        contenido:'',
        tarjetas: [
          {
            titulo: 'Vocal A',
            espanol: 'Sembrar',
            mam: 'Awal',
            imagen: 'img/contenido/Picture35.png',
          },{
            titulo: 'Vocal E',
            espanol: 'Esconderse',
            mam: 'Ewil',
            imagen: 'img/contenido/Picture5.png',
          },{
            titulo: 'Vocal I',
            espanol: 'Chile',
            mam: 'Ich',
            imagen: 'img/contenido/Picture6.png',
          },{
            titulo: 'Vocal O',
            espanol: 'Aguacate',
            mam: 'Oj',
            imagen: 'img/contenido/Picture36.png',
          },{
            titulo: 'Vocal U',
            espanol: 'Mosca',
            mam: 'Us',
            imagen: 'img/contenido/Picture8.png',
          }
        ]},
      {titulo: 'Las Consonantes',
        contenido:'',
        tarjetas: [
          {
            titulo: 'Consonante J',
            espanol: 'Masorca',
            mam: 'Jal',
            imagen: 'img/contenido/Picture9.png',
          },
          {
            titulo: 'Consonante K',
            espanol: 'Piedra de moler',
            mam: 'Ka’',
            imagen: 'img/contenido/Picture10.png',
          },
          {
            titulo: 'Consonante L',
            espanol: 'Chichicaste',
            mam: 'La',
            imagen: 'img/contenido/Picture11.png',
          },
          {
            titulo: 'Consonante M',
            espanol: 'Rastrojo',
            mam: 'Maj',
            imagen: 'img/contenido/Picture12.png',
          },
          {
            titulo: 'Consonante N',
            espanol: 'Lleno',
            mam: 'Nonji',
            imagen: 'img/contenido/Picture13.png',
          },
          {
            titulo: 'Consonante P',
            espanol: 'Petate',
            mam: 'Pop',
            imagen: 'img/contenido/Picture14.png',
          },
          {
            titulo: 'Consonante Q',
            espanol: 'Cortar',
            mam: 'Quesir',
            imagen: 'img/contenido/Picture15.png',
          },{
            titulo: 'Consonante R',
            espanol: 'Carnero',
            mam: 'Rit',
            imagen: 'img/contenido/Picture15.png',
          },{
            titulo: 'Consonante S',
            espanol: 'Cicrulo',
            mam: 'Sewin',
            imagen: 'img/contenido/Picture16.png',
          },{
            titulo: 'Consonante T',
            espanol: 'Cilindrico',
            mam: 'Tolin',
            imagen: 'img/contenido/Picture17.png',
          },{
            titulo: 'Consonante W',
            espanol: 'Gato',
            mam: 'Wiẍ',
            imagen: 'img/contenido/Picture18.png',
          },{
            titulo: 'Consonante X',
            espanol: 'Jarro',
            mam: 'Xar',
            imagen: 'img/contenido/Picture19.png',
          },{
            titulo: 'Consonante Ẍ',
            espanol: 'Conejo',
            mam: 'Ẍiky',
            imagen: 'img/contenido/Picture20.png',
          },{
            titulo: 'Consonante Y',
            espanol: 'Escalera',
            mam: 'yotx',
            imagen: 'img/contenido/Picture21.png',
          },{
            titulo: 'Consonante ’',
            espanol: 'Achote',
            mam: 'o’x',
            imagen: 'img/contenido/Picture22.png',
          }
        ]
      },
        {titulo: 'Uso de las mayúsculas ',
          contenido:'Se escribe con mayúscula la letra inicial de nombres propios, ya sea de personas, animales o lugares; y también la letra inicial de los apellidos.',
          imagenes: [
            'img/contenido/nombres-may.png'
          ]
        },
      {titulo: 'Uso de la coma', contenido:'<p>La coma, en el idioma Mam tiene varios usos. A continuaci&oacute;n, se muestran algunas normas.</p>\n' +
          '\n' +
          '<p><strong>a) Si dentro de una oraci&oacute;n se quiere agregar informaci&oacute;n sobre algo o alguien (en aposici&oacute;n), esta informaci&oacute;n se debe separar del sustantivo utilizando coma (,).</strong></p>\n' +
          '\n' +
          '<p>Por ejemplo:</p>\n' +
          '\n' +
          '<p>A k&rsquo;wal<strong>,&nbsp;</strong>wa&rsquo;li tza&rsquo;lu<strong>,&nbsp;</strong>ma yolin wuk&rsquo;ile.</p>\n' +
          '\n' +
          '<p>El ni&ntilde;o, que est&aacute; parado ac&aacute;, habl&oacute; conmigo.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Atzun k&rsquo;wal<strong>,&nbsp;</strong>aj saq t-xb&rsquo;alin atok<strong>,&nbsp;</strong>atzun saj onin weye.</p>\n' +
          '\n' +
          '<p>El ni&ntilde;o, que tiene ropa blanca, fue quien me ayud&oacute;.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p><strong>b) Se usa coma cuando la oraci&oacute;n empieza con una explicaci&oacute;n sobre cu&aacute;ndo ocurre la acci&oacute;n.&nbsp;</strong></p>\n' +
          '\n' +
          '<p>Ejemplo:</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Tej tula<strong>,&nbsp;</strong>in tzalaje.</p>\n' +
          '\n' +
          '<p>Cuando viniste, me puse feliz.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Ok tula<strong>,&nbsp;</strong>ok qo xe&rsquo;l loq&rsquo;il.</p>\n' +
          '\n' +
          '<p>Cuando vengas, iremos a comprar.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p><strong>c) Se usa coma para separar una secuencia de oraciones simples.&nbsp;</strong></p>\n' +
          '\n' +
          '<p>Por ejemplo:</p>\n' +
          '\n' +
          '<p>Ok npona njaya<strong>,&nbsp;</strong>ok chin saqchala<strong>,&nbsp;</strong>ok chin onila ti&rsquo;j ntxuya<strong>,&nbsp;</strong>ok chin b&rsquo;inchal waq&rsquo;una ex ok chin u&rsquo;jila.</p>\n' +
          '\n' +
          '<p>Cuando llegue a mi casa, jugar&eacute;, ayudar&eacute; a mi mam&aacute;, har&eacute; mis tareas y leer&eacute;.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Ma&nbsp;chinxe&rsquo;&nbsp;toj&nbsp;jun&nbsp;chmb&rsquo;il,&nbsp;aj&nbsp;npone,&nbsp;ok&nbsp;chin&nbsp;yolile,&nbsp;ok&nbsp;chin&nbsp;ximile,&nbsp;ok&nbsp;chin&nbsp;xpich&rsquo;ile&nbsp;ex&nbsp;kb&rsquo;el ntz&rsquo;ib&rsquo;in&nbsp;nxime.</p>\n' +
          '\n' +
          '<p>Voy a una reuni&oacute;n y cuando llegue, hablar&eacute;, analizar&eacute;, invetigar&eacute; y dejar&eacute; escrito me propuesta.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p><strong>d) Se usa coma para separar los elementos de una enumeraci&oacute;n.</strong></p>\n' +
          '\n' +
          '<p>&nbsp;Ejemplo:</p>\n' +
          '\n' +
          '<p>Ma chinx anja toj k&rsquo;ayb&rsquo;il ex ma chi tzaj nloq&rsquo;one qe&rsquo; nlyo&rsquo;ye: chul<strong>,&nbsp;</strong>jatze<strong>,&nbsp;</strong>matzti&rsquo;j ex chapa&rsquo;l.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Fui al mercado y compr&eacute;: zapotes, matasanos, pi&ntilde;as y nances.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Nimqe txkup ate&rsquo; twitz tx&rsquo;otx&rsquo;, se&rsquo;nqe: tx&rsquo;yan, patz, chej ex wakx.</p>\n' +
          '\n' +
          '<p>Existen muchos animales en la naturaleza, tales como: perros, patos, caballos y vacas.</p>\n'},
      {titulo: 'El uso del punto', contenido:'<p>Representa la pausa que se da al final de una oraci&oacute;n. Despu&eacute;s de punto siempre se escribe may&uacute;scula.</p>\n' +
          '\n' +
          '<p>Ilti&rsquo;j tu&rsquo;n qyolin toj qyol tu&rsquo;n mi&rsquo;n kub&rsquo; naj. B&rsquo;a&rsquo;n tu&rsquo;n qyolin toj tnam, toj qja, toj qaq&rsquo;un ex jachaq tumel ato&rsquo;.</p>\n' +
          '\n' +
          '<p>Es importante hablar en el idioma materno para que no desaparezca. Hablemos en la ciudad, en la casa, en el trabajo y donde estemos.</p>\n' +
          '\n' +
          '<p>El punto tambi&eacute;n puede representar el cambio de p&aacute;rrafo. Este uso del punto se le conoce como punto y aparte.&nbsp;</p>\n' +
          '\n' +
          '<p>B&rsquo;i&rsquo;n&nbsp;qu&rsquo;n&nbsp;at&nbsp;tanma&nbsp;tx&rsquo;otx&rsquo;&nbsp;ex&nbsp;qa&nbsp;at&nbsp;tanma&nbsp;ok&nbsp;tb&rsquo;aj&nbsp;qtoch&rsquo;i&rsquo;n&nbsp;twitz.&nbsp;Kye&nbsp;tzun&nbsp;chmanb&rsquo;aj&nbsp;ojtxi, nimtaq xo&rsquo;n kyu&rsquo;n ajo twitz tx&rsquo;otx&rsquo;, ex na&rsquo;ntaq kyu&rsquo;n ti&rsquo;chaq aq&rsquo;unlt tu&rsquo;n t-xi&rsquo; kyb&rsquo;inchan, tu&rsquo;ntzun min kyb&rsquo;aj xpitz&rsquo;in tu&rsquo;n tanma&nbsp;tx&rsquo;otx&rsquo;.</p>\n' +
          '\n' +
          '<p>Sabemos sobre la energ&iacute;a de la madre tierra y perjudicamos la energ&iacute;a cuando se remueve. Mientras los abuelos antes respetaban la naturaleza y tienen en mente los trabajos que se hac&iacute;a para no mal tratar su energ&iacute;a.</p>\n' +
          '\n' +
          '<p>Ok&nbsp;t-xi&rsquo;&nbsp;kyxima&rsquo;n&nbsp;chmanb&rsquo;aj&nbsp;ok&nbsp;tjaw&nbsp;jun&nbsp;kyja,&nbsp;ilti&rsquo;j&nbsp;tu&rsquo;n&nbsp;kyximan&nbsp;ti&rsquo;j&nbsp;jun&nbsp;qanb&rsquo;il&nbsp;te&nbsp;twitz&nbsp;tx&rsquo;otx&rsquo;, tu&rsquo;n tzaj q&rsquo;o&rsquo;n amb&rsquo;il te tajwil ja tu&rsquo;n t-xi&rsquo; qe&nbsp;taq&rsquo;un.</p>\n' +
          '\n' +
          '<p>Al construir una casa, los abuelos pensaban importante sobre el legar de la vivienda para luego empezar a construir dicha casa.</p>\n'},
      {titulo: 'Uso de dos puntos', contenido:'<p>Detienen el discurso para llamar la atenci&oacute;n sobre lo que sigue.</p>\n' +
          '\n' +
          '<p>Se usan los dos puntos en los casos siguientes: Despu&eacute;s de anunciar una enumeraci&oacute;n. Ejemplo:</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>At kyaj tnam ja nchi yolanjtz qyol: Chnab&rsquo;jul, T-xeljub&rsquo;, T-xe Chman ex toj Mlaj.</p>\n' +
          '\n' +
          '<p>&ldquo;El idioma Mam, se habla en cuatro departamentos: Huehuetenango, Quetzaltenango, San Marcos y Retalhuleu.&rdquo;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>b) Para cerrar una enumeraci&oacute;n, antes del anaf&oacute;rico (significado) que los sustituye,&nbsp;se&nbsp;utilizan los dos puntos.&nbsp;Ejemplo:</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>At nim tzalajsb&rsquo;il, chwinqlal ex numb&rsquo;il: ma mojb&rsquo;et kyxim qxjalil&nbsp;</p>\n' +
          '\n' +
          '<p>Hay mucha felicidad, vida y paz: La gente uni&oacute; su pensamiento.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>c) Verificaci&oacute;n o explicaci&oacute;n de la proposici&oacute;n anterior, que suele tener un sentido m&aacute;s general. Por&nbsp;ejemplo:</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Tb&rsquo;anil makax te qchi&rsquo;: nchi jaw ch&rsquo;iy tuj tx&rsquo;otx&rsquo;, at kytxamnil ex tb&rsquo;anil sik&rsquo; te qximlal.</p>\n' +
          '\n' +
          '<p>&ldquo;Los zompopos son muy buenos para nuestra comida: salen de la &nbsp;tierra, son grasosos y muy alimenticios para nuestro organismo.&rdquo;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Uso del signo de Interrogaci&oacute;n&nbsp;</p>\n' +
          '\n' +
          '<p>Algunas preguntas en espa&ntilde;ol:&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&iquest;A d&oacute;nde&nbsp;fuiste?</p>\n' +
          '\n' +
          '<p>&iquest;Qu&eacute; har&aacute;s hoy en la&nbsp;tarde?</p>\n' +
          '\n' +
          '<p>&iquest;C&oacute;mo se llama su&nbsp;pap&aacute;?</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Ahora bien, usted se ha fijado que en espa&ntilde;ol hay un signo de interrogaci&oacute;n que abre&nbsp;la pregunta y una que la cierra. En Mam solo se utiliza el signo que cierra la pregunta. Observe los ejemplos&nbsp;siguientes:</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<table border="1" cellspacing="0">\n' +
          '\t<tbody>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="50%">\n' +
          '\n' +
          '\t\t\t\t<p>Ma wa&rsquo;na?</p>\n' +
          '\n' +
          '\t\t\t\t<p>Ma kub&rsquo; ttx&rsquo;emina tze? Jatzan ma&nbsp;txi&rsquo;ya?</p>\n' +
          '\n' +
          '\t\t\t\t<p>Ma tzula?</p>\n' +
          '\n' +
          '\t\t\t\t<p>&nbsp;</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="50%">\n' +
          '\n' +
          '\t\t\t\t<p>&iquest;Ya comi&oacute;?</p>\n' +
          '\n' +
          '\t\t\t\t<p>&iquest;Cort&oacute; el &aacute;rbol?</p>\n' +
          '\n' +
          '\t\t\t\t<p>&iquest;A d&oacute;nde va?</p>\n' +
          '\n' +
          '\t\t\t\t<p>&iquest;Ya vino?</p>\n' +
          '\n' +
          '\t\t\t\t<p>&nbsp;</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t</tbody>\n' +
          '</table>\n' +
          '\n' +
          '<p>&nbsp;</p>\n'},
      {titulo: 'Uso de los signos de exclamación ', contenido:'<p>Los signos de exclamaci&oacute;n (&iexcl;!) encierran enunciados que exclaman. A diferencia del signo de interrogaci&oacute;n, en este caso s&iacute; se utilizan tanto el signo que abre como el que cierra la expresi&oacute;n de exclamaci&oacute;n.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Mya&rsquo;&nbsp;b&rsquo;a&rsquo;n&nbsp;ikyjo!&iexcl;Eso no est&aacute; bien!</p>\n' +
          '\n' +
          '<p>&iexcl;Tb&rsquo;anil&nbsp;ka&rsquo;yin&nbsp;b&rsquo;ech!&iexcl;Qu&eacute; bonita&nbsp;flor!</p>\n' +
          '\n' +
          '<p>&iexcl;Aaa! &iexcl;Eee! &iexcl;Oooo!</p>\n' +
          '\n' +
          '<p>&iexcl;Kutzaj!&iexcl;Ven!</p>\n' +
          '\n' +
          '<p>&iexcl;Ka&rsquo;yinxa!&iexcl;M&iacute;ralo!</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n'},
      {titulo: 'Corte silábico', contenido:'<p>Existen&nbsp;casos&nbsp;en&nbsp;los&nbsp;que&nbsp;necesitamos&nbsp;dividir&nbsp;una&nbsp;palabra&nbsp;en&nbsp;s&iacute;labas,&nbsp;pero&nbsp;&iquest;en&nbsp;qu&eacute;&nbsp;parte la&nbsp;dividimos?</p>\n' +
          '\n' +
          '<p><strong>&nbsp;</strong></p>\n' +
          '\n' +
          '<p>A continuaci&oacute;n, le damos algunas reglas para hacer esta divisi&oacute;n.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<ul>\n' +
          '\t<li>a)&nbsp;La existencia de una vocal implica la existencia de una&nbsp;s&iacute;laba. Ejemplos:</li>\n' +
          '</ul>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<table border="0" cellspacing="0">\n' +
          '\t<tbody>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="30.48016701461378%">\n' +
          '\n' +
          '\t\t\t\t<p>q&#39;ob&#39;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="39.87473903966597%">\n' +
          '\n' +
          '\t\t\t\t<p>q&#39;o-b&#39;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="29.64509394572025%">\n' +
          '\n' +
          '\t\t\t\t<p>&ldquo;brazo-mano&rdquo;</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="30.48016701461378%">\n' +
          '\n' +
          '\t\t\t\t<p>jib&rsquo;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="39.87473903966597%">\n' +
          '\n' +
          '\t\t\t\t<p>ji-b&rsquo;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="29.64509394572025%">\n' +
          '\n' +
          '\t\t\t\t<p>suegro</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="30.48016701461378%">\n' +
          '\n' +
          '\t\t\t\t<p>tzib&rsquo;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="39.87473903966597%">\n' +
          '\n' +
          '\t\t\t\t<p>tzi-b&rsquo;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="29.64509394572025%">\n' +
          '\n' +
          '\t\t\t\t<p>boca</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="30.48016701461378%">\n' +
          '\n' +
          '\t\t\t\t<p>txamnil</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="39.87473903966597%">\n' +
          '\n' +
          '\t\t\t\t<p>txam-nil</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="29.64509394572025%">\n' +
          '\n' +
          '\t\t\t\t<p>grasa</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t</tbody>\n' +
          '</table>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<ul>\n' +
          '\t<li>b)&nbsp;En palabras de m&aacute;s de una s&iacute;laba, cuyas vocales sean separadas por una sola consonante, &eacute;sta &uacute;ltima pasa a formar parte de la &uacute;ltima&nbsp;s&iacute;laba.</li>\n' +
          '</ul>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<table border="0" cellspacing="0">\n' +
          '\t<tbody>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="33.26039387308534%">\n' +
          '\n' +
          '\t\t\t\t<p>ewi</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="42.4507658643326%">\n' +
          '\n' +
          '\t\t\t\t<p>e-wi</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="24.288840262582056%">\n' +
          '\n' +
          '\t\t\t\t<p>ayer</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="33.26039387308534%">\n' +
          '\n' +
          '\t\t\t\t<p>nwatb&#39;ile</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="42.4507658643326%">\n' +
          '\n' +
          '\t\t\t\t<p>nwat-b&#39;i-le</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="24.288840262582056%">\n' +
          '\n' +
          '\t\t\t\t<p>mi cama</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="33.26039387308534%">\n' +
          '\n' +
          '\t\t\t\t<p>ewaj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="42.4507658643326%">\n' +
          '\n' +
          '\t\t\t\t<p>e-waj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="24.288840262582056%">\n' +
          '\n' +
          '\t\t\t\t<p>escondida</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="33.26039387308534%">\n' +
          '\n' +
          '\t\t\t\t<p>itzaj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="42.4507658643326%">\n' +
          '\n' +
          '\t\t\t\t<p>i-tzaj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="24.288840262582056%">\n' +
          '\n' +
          '\t\t\t\t<p>hierba</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="33.26039387308534%">\n' +
          '\n' +
          '\t\t\t\t<p>ula</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="42.4507658643326%">\n' +
          '\n' +
          '\t\t\t\t<p>u-la</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="24.288840262582056%">\n' +
          '\n' +
          '\t\t\t\t<p>visitante</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="33.26039387308534%">\n' +
          '\n' +
          '\t\t\t\t<p>&nbsp;</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="42.4507658643326%">\n' +
          '\n' +
          '\t\t\t\t<p>&nbsp;</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="24.288840262582056%">\n' +
          '\n' +
          '\t\t\t\t<p>&nbsp;</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t</tbody>\n' +
          '</table>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<ul>\n' +
          '\t<li>c)&nbsp;En las palabras donde las vocales sean separadas por dos consonantes, cada una de &eacute;stas forma parte de s&iacute;labas distintas.</li>\n' +
          '</ul>\n' +
          '\n' +
          '<p>Ejemplos:</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<table border="0" cellspacing="0">\n' +
          '\t<tbody>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="29.343629343629345%">\n' +
          '\n' +
          '\t\t\t\t<p>xnaq&#39;tzal</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="36.87258687258687%">\n' +
          '\n' +
          '\t\t\t\t<p>xnaq&#39;-tzal</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="33.78378378378378%">\n' +
          '\n' +
          '\t\t\t\t<p>ense&ntilde;ar / aprender</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="29.343629343629345%">\n' +
          '\n' +
          '\t\t\t\t<p>Paqli</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="36.87258687258687%">\n' +
          '\n' +
          '\t\t\t\t<p>paq-li</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="33.78378378378378%">\n' +
          '\n' +
          '\t\t\t\t<p>echado</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="29.343629343629345%">\n' +
          '\n' +
          '\t\t\t\t<p>kub&#39;ni</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="36.87258687258687%">\n' +
          '\n' +
          '\t\t\t\t<p>kub&#39;-ni</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="33.78378378378378%">\n' +
          '\n' +
          '\t\t\t\t<p>hacia abajo</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="29.343629343629345%">\n' +
          '\n' +
          '\t\t\t\t<p>qanb&rsquo;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="36.87258687258687%">\n' +
          '\n' +
          '\t\t\t\t<p>qan-b&rsquo;aj</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="33.78378378378378%">\n' +
          '\n' +
          '\t\t\t\t<p>pie</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t</tbody>\n' +
          '</table>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<ul>\n' +
          '\t<li>d)&nbsp;En los casos donde ocurren tres consonantes juntas en medio de dos vocales, se separan de la siguiente manera:</li>\n' +
          '</ul>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Primer caso, cuando la combinaci&oacute;n&nbsp;VCCLV&nbsp;(vocal+consonante+consonante+ele+vocal), CL no se puede dividir y forman parte de&nbsp;la misma&nbsp;s&iacute;laba.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Ejemplo:</p>\n' +
          '\n' +
          '<p>Chinkli&rsquo;chin-kli&#39;chicharra</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>Segundo&nbsp;caso:&nbsp;las&nbsp;combinaciones&nbsp;de&nbsp;tres&nbsp;consonantes&nbsp;que&nbsp;no&nbsp;incluyen&nbsp;/l/,&nbsp;o&nbsp;sea&nbsp;VCCCV se deben separar as&iacute; VC-CCV, es decir, que la primera consonante pertenece a la primera s&iacute;laba, mientras que las otras dos pertenecen a la siguiente&nbsp;s&iacute;laba.</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<table border="0" cellspacing="0">\n' +
          '\t<tbody>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="23.223570190641247%">\n' +
          '\n' +
          '\t\t\t\t<p>Ejemplo:</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td colspan="2" valign="top" width="76.77642980935875%">\n' +
          '\n' +
          '\t\t\t\t<p>&nbsp;</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="23.26388888888889%">\n' +
          '\n' +
          '\t\t\t\t<p>Xnaq&rsquo;tzb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="32.8125%">\n' +
          '\n' +
          '\t\t\t\t<p>xnaq&#39;-tzb&#39;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="43.923611111111114%">\n' +
          '\n' +
          '\t\t\t\t<p>Ense&ntilde;anza</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="23.26388888888889%">\n' +
          '\n' +
          '\t\t\t\t<p>B&rsquo;ajsb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="32.8125%">\n' +
          '\n' +
          '\t\t\t\t<p>b&rsquo;aj-sb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="43.923611111111114%">\n' +
          '\n' +
          '\t\t\t\t<p>Terminaci&oacute;n</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="23.26388888888889%">\n' +
          '\n' +
          '\t\t\t\t<p>Chewsb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="32.8125%">\n' +
          '\n' +
          '\t\t\t\t<p>chew-sb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="43.923611111111114%">\n' +
          '\n' +
          '\t\t\t\t<p>instrumento para enfriar</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="23.26388888888889%">\n' +
          '\n' +
          '\t\t\t\t<p>Moq&rsquo;sb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="32.8125%">\n' +
          '\n' +
          '\t\t\t\t<p>moq&rsquo;-sb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="43.923611111111114%">\n' +
          '\n' +
          '\t\t\t\t<p>instrumento para canlentar</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t\t<tr>\n' +
          '\t\t\t<td valign="top" width="23.26388888888889%">\n' +
          '\n' +
          '\t\t\t\t<p>K&rsquo;ansb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="32.8125%">\n' +
          '\n' +
          '\t\t\t\t<p>k&rsquo;an-sb&rsquo;il</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t\t<td valign="top" width="43.923611111111114%">\n' +
          '\n' +
          '\t\t\t\t<p>instrumento para encender</p>\n' +
          '\t\t\t</td>\n' +
          '\t\t</tr>\n' +
          '\t</tbody>\n' +
          '</table>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n' +
          '\n' +
          '<p>&nbsp;</p>\n'}
    ]
  }
};
