window.contenidoTestJson = {
    "1": [
        {
            titulo: 'Seleccione la respuesta correcta de las palabras en mam.',
            multiple: [
                {
                    titulo: "piedra de moler",
                    opciones: [
                        "Ka’","kab’eje","kukẍ","ku’k"
                    ],
                    respuesta: "Ka’"
                },
                {
                    titulo: "Lleno",
                    opciones: [
                        "nach", "chnab’", "nojni","jun"
                    ],
                    respuesta: "nojni"
                },
                {
                    titulo: "circulo",
                    opciones: [
                        "sakul","sewin","sasj","is"
                    ],
                    respuesta: "sewin"
                },
                {
                    titulo: "conejo",
                    opciones: [
                        "ẍal","kyiẍil","i’ẍ","ẍiky"
                    ],
                    respuesta: "ẍiky"
                },
                {
                    titulo: "cielo",
                    opciones: [
                        "kyaj","kya’j","tzikyb’aj","piky"
                    ],
                    respuesta: "kya’j"
                }
            ]
        }
    ]
}